module.exports = {
    entry: './src/index.ts',
    output: {
        path: __dirname + "/dist",
        filename: "index.js"
    },
    resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
    },
    module: {
        loaders: [
            {test: /\.tsx?$/, loader: 'ts-loader'}
        ]
    },
    devtool: 'source-map'
};