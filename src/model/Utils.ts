import * as _ from "lodash";

export function debounce(time: number): Function {
    return function (target, key, descriptor): void {
        descriptor.value = _.debounce(descriptor.value, time);
        return descriptor
    }
}
