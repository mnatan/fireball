import * as ex from "excalibur";
import {Resources} from "../game/Resources";
import {DestructableActor, DamageType} from "../intefraces/DescructableActor";
import {HpBar} from "../UI/HpBar";
import {Game} from "../game/Game";
import {debounce} from "./Utils";
import {Animation} from "Drawing/Animation";

export class Monster extends DestructableActor {
    animation: Animation;
    private anim_stand: Animation;
    private anim_die: Animation;

    constructor(x: number, y: number) {
        super(x, y, 50, 50);

        this.health = 200;
        this.hpBar = new HpBar(x, y, this.health);
        this.collisionType = ex.CollisionType.Active;

        let sprite_size = 64;
        let scale = 50 / sprite_size;
        this.anim_stand = new ex.SpriteSheet(Resources.resources.skeleton, 4, 1, sprite_size, sprite_size)
            .getAnimationForAll(Game.engine, 150);
        this.anim_stand.scale = new ex.Vector(scale, scale);
        this.addDrawing('stand', this.anim_stand);

        this.anim_die = new ex.SpriteSheet(Resources.resources.skeleton, 7, 4, sprite_size, sprite_size)
            .getAnimationBetween(Game.engine, 21, 29, 150);
        this.anim_die.scale = new ex.Vector(scale, scale);
        this.anim_die.loop = false;
        this.addDrawing('die', this.anim_die);

        this.setDrawing('stand');

    }

    @debounce(10)
    take_damage(damage: number, type: DamageType) {
        Game.engine.add(this.hpBar);
        this.health -= damage;
        this.hpBar.hp = this.health;
        if (this.health < 0) {
            this.collisionType = ex.CollisionType.PreventCollision;
            this.setDrawing('die');
            this.hpBar.kill()
        }
    }

    update(engine: ex.Engine, delta: number) {
        if (this.anim_die.isDone()) {
            this.kill();
        }
    }
}
