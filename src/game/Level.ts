import * as ex from "excalibur";
import {getAction} from "../utils/utils";
import {Wall} from "../model/Wall";
import {Player} from "../model/Player";
import {Floor} from "../model/Floor";
import {Monster} from "../model/Monster";


export class Level {
    constructor(data: string, game: ex.Engine, player: Player) {

        game.currentScene.camera = new ex.LockedCamera();
        game.currentScene.camera.setActorToFollow(player);

        let objectLayer: ex.Actor[] = [];
        objectLayer.push(player);

        let lines = data.split('\n');
        for (let y = 0; y < lines.length; y++) {
            for (let x = 0; x < lines[y].length; x++) {

                let actions = {
                    '#': () => new Wall(x * 50, y * 50),
                    '.': () => new Floor(x * 50, y * 50),
                    'P': () => {
                        player.x = x * 49;
                        player.y = y * 49;
                        return new Floor(x * 50, y * 50)
                    },
                    'm': () => {
                        objectLayer.push(new Monster(x * 50, y * 50));
                        return new Floor(x * 50, y * 50)
                    }
                };

                game.add(getAction(actions, lines[y][x])());
            }
        }
        objectLayer.forEach(x => game.add(x));
    }
}